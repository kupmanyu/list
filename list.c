#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <limits.h>

//Error constants for printing
enum { ForwardError, BackwardError, InsertBError, InsertAError, GetBError, GetAError, SetBError, SetAError, DelBError, DelAError  };

//Cell structure containing the value, and pointers to next and previous values
struct cell {
  item value;
  struct cell *next;
  struct cell *previous;
};
typedef struct cell cell;

//Structure which keeps track of the current cell
struct list {
  cell *current;
};

void forward(list *l);
void backward(list *l);
void printError(int x);

//Create an empty new list
list *newList() {
  list *new = malloc(sizeof(list));
  new->current = NULL;
  return new;
}

//Pointer to current cell becomes the first cell in the list
void start(list *l) {
  cell *c = l->current;
  while (c->previous != NULL) {
    backward(l);
    c = l->current;
  }
}

//Pointer to current cell becomes last cell in list;
void end(list *l) {
  cell *c = l->current;
  while (c->next != NULL) {
    forward(l);
    c = l->current;
  }
}

//Checks if pointer is to first cell in list
bool atStart(list *l) {
  cell *current = l->current;
  if (current->previous == NULL) return 1;
  else return 0;
}

//Checks if pointer is to last cell in list
bool atEnd(list *l) {
  cell *current = l->current;
  if (current->next == NULL) return 1;
  else return 0;
}

//Moves the pointer to the next cell
void forward(list *l) {
  cell *c = l->current;
  if (c->next == NULL) printError(ForwardError);
  else l->current = c->next;
}

//Moves the pointer to the previous cell
void backward(list *l) {
  cell *c = l->current;
  if (c->previous == NULL) printError(BackwardError);
  else l->current = c->previous;
}

//Inserts a new cell between the current cell and the previous cell
void insertBefore(list *l, item x) {
  cell *c = l->current;
  if (l->current == NULL) printError(InsertBError);
  else if (c->previous == NULL) {
    cell *new = malloc(sizeof(cell));
    *new = (cell) {x, l->current, NULL};
    c->previous = new;
    backward(l);
  }
  else {
    cell *new = malloc(sizeof(cell));
    *new = (cell) {x, l->current, c->previous};
    c->previous = new;
    backward(l);
    backward(l);
    c = l->current;
    c->next = new;
    forward(l);
  }
}

//Inserts a new cell between the current cell and the next cell
void insertAfter(list *l, item x) {
    cell *c = l->current;
    if (l->current == NULL) {
      cell *new = malloc(sizeof(cell));
      *new = (cell) {x, NULL, NULL};
      l->current = new;
    }
    else if (c->next == NULL){
      cell *new = malloc(sizeof(cell));
      *new = (cell) {x, NULL, l->current};
      c->next = new;
      forward(l);
    }
    else {
      cell *new = malloc(sizeof(cell));
      *new = (cell) {x, c->next, l->current};
      c->next = new;
      forward(l);
      forward(l);
      c = l->current;
      c->previous = new;
      backward(l);
    }
}

//Returns the value in the previous cell
item getBefore(list *l) {
  cell *c = l->current;
  if (atStart(l) == 1) {
    printError(GetBError);
    return GetBError;
  }
  else {
    c = c->previous;
    return c->value;
  }
}

//Returns the value in the next cell
item getAfter(list *l) {
  cell *c = l->current;
  if (atEnd(l) == 1) {
    printError(GetAError);
    return GetAError;
  }
  else {
    c = c->next;
    return c->value;
  }
}

//Changes the value in the previous cell
void setBefore(list *l, item x) {
  cell *c = l->current;
  if (c->previous != NULL) {
    backward(l);
    c = l->current;
    c->value = x;
    forward(l);
  }
  else printError(SetBError);
}

//Changes the value in the next cell
void setAfter(list *l, item x) {
  cell *c = l->current;
  if (c->next != NULL) {
    forward(l);
    c = l->current;
    c->value = x;
    backward(l);
  }
  else printError(SetAError);
}

//Deletes the previous cell from the list
void deleteBefore(list *l) {
  cell *c = l->current;
  if (c->previous == NULL) printError(DelBError);
  else {
    cell *del = malloc(sizeof(cell));
    cell *next;
    cell *prev;
    next = l->current;
    backward(l);
    del = l->current;
    backward(l);
    prev = l->current;
    prev->next = next;
    forward(l);
    c = l->current;
    c->previous = prev;
    free(del);
  }
}

//Deletes the next cell from the list
void deleteAfter(list *l) {
  cell *c = l->current;
  if (c->next == NULL) printError(DelAError);
  else {
    cell *del = malloc(sizeof(cell));
    cell *next;
    cell *prev;
    prev = l->current;
    forward(l);
    del = l->current;
    forward(l);
    next = l->current;
    next->previous = prev;
    backward(l);
    c = l->current;
    c->next = next;
    free(del);
  }
}

//-----------------------------------------------------------------------------
//Testing

//Function to compare the state of the list with a string, and check whether the
//pointer is pointing to the correct current position
bool compare(list *l, char *lstate, item x) {
  cell *c = l->current;
  bool correctlist = 0;
  bool correctposition = 0;
  int length = strlen(lstate);
  if (x == c->value) correctposition = 1;
  cell *currentposition = l->current;
  start(l);
  c = l->current;
  for (size_t i = 0; i < length; i++) {
    if ((lstate[i] - '0') == c->value) {
      correctlist = 1;
      if (c->next != NULL) forward(l);
    }
    else {
      correctlist = 0;
      break;
    }
    c = l->current;
  }
  l->current = currentposition;
  return (correctlist & correctposition);
}

//Function to print the list
//void printList(list *l) {
//  cell *c = l->current;
//  while(c->next != NULL) {
//    printf("%d, ", c->value);
//    forward(l);
//    c = l->current;
//  }
//  printf("%d\n", c->value);
//}

//Function that prints the errors
void printError(int x) {
  if (x == ForwardError) printf("Cannot go forward\n");
  if (x == BackwardError) printf("Cannot go backward\n");
  if (x == InsertBError) printf("Cannot insert before node\n");
  if (x == InsertAError) printf("Cannot insert after node\n");
  if (x == GetAError) printf("Cannot get value in next node\n");
  if (x == GetBError) printf("Cannot get value in previous node\n");
  if (x == SetAError) printf("Cannot set value in next node\n");
  if (x == SetBError) printf("Cannot set value in previous node\n");
  if (x == DelBError) printf("Cannot delete value in previous node\n");
  if (x == DelAError) printf("Cannot delete value in next node\n");
}

//Function that calls all the tests
void test() {
  list *l;
  l = newList();
  insertAfter(l, 0);
  insertAfter(l, 1);
  insertAfter(l, 2);
  insertAfter(l, 3);
  assert(compare(l, "0123", 3) == 1);
  backward(l);
  assert(compare(l, "0123", 2) == 1);
  insertAfter(l, 4);
  assert(compare(l, "01243", 4) == 1);
  insertBefore(l, 5);
  assert(compare(l, "012543", 5) == 1);
  assert(atStart(l) == 0);
  start(l);
  assert(compare(l, "012543", 0) == 1);
  assert(atStart(l) == 1);
  insertBefore(l, 6);
  assert(compare(l, "6012543", 6) == 1);
  forward(l);
  //assert(getBefore(l) == GetBError);
  assert(getAfter(l) == 1);
  setAfter(l, 2);
  assert(compare(l, "6022543", 0) == 1);
  //setBefore(l, 1);
  deleteAfter(l);
  assert(compare(l, "602543", 0) == 1);
  //deleteBefore(l);
  assert(atEnd(l) == 0);
  end(l);
  assert(compare(l, "602543", 3) == 1);
  assert(atEnd(l) == 1);
  assert(getBefore(l) == 4);
  //assert(getAfter(l) == GetAError);
  setBefore(l, 7);
  assert(compare(l, "602573", 3) == 1);
  //setAfter(l, 1);
  deleteBefore(l);
  assert(compare(l, "60253", 3) == 1);
  //deleteAfter(l);
  start(l);
  //printList(l);
  cell *x = l->current;
  while(l->current->next != NULL) {
    forward(l);
    free(x);
    x = l->current;
  }
  free(x);
  free(l);
  printf("All tests pass\n");
}

int listMain() {
  test();
}
